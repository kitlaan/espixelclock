#ifndef SENSORS_H
#define SENSORS_H

#include <DS3231M.h>

class WallClock {
public:
    WallClock();
    ~WallClock();

    void setup();

    bool isSynced() const;

    void setTimeZone(const String &tz);
    void setEndpoints(const String &host1, const String &host2);

    void set24h(bool enable);

    void refresh();

    uint16_t getTimeDigits();
    bool isMarketOpen();

private:
    DS3231M_Class m_rtc;

    bool m_needsUpdate;
    bool m_isSynced;

    bool m_24h;

    String m_tz;
    String m_host1;
    String m_host2;
};

extern WallClock wallclock;

///////////////////////////////////////////////////////////

#include <HTU21D.h>

class Weather {
public:
    Weather();
    ~Weather();

    void setup();

    void refresh();

    float getTemperature() const;
    float getHumidity() const;

private:
    HTU21D m_sensor;

    float m_temp;
    float m_humid;
};

extern Weather weather;

#endif // SENSORS_H
#ifndef STOCK_H
#define STOCK_H

#include <WString.h>

class StockTracker {
public:
    StockTracker();
    ~StockTracker();

    void setSymbol(const String& symbol);

    bool fetch();

    float getPrice() const;
    float getScale() const;

    bool  getFailed() const;
    bool  getMarketOpen() const;
    int   getLastHttp() const;

private:
    String   m_symbol;
    uint32_t m_failCount;
    uint32_t m_sameCount;
    bool     m_marketOpen;

    int      m_lastHttpCode;

    struct StockData {
        float prevClosePrice;
        float openPrice;
        float currPrice;
        float highPrice;
        float lowPrice;
    };
    StockData m_prevData;
    StockData m_currData;
};

extern StockTracker stock;

#endif // STOCK_H
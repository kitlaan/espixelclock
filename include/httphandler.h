#ifndef HTTPHANDLER_H
#define HTTPHANDLER_H

class AsyncWebServer;

void setupHttp(AsyncWebServer& server);

#endif // HTTPHANDLER_H
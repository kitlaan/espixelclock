#ifndef LEDCLOCK_H
#define LEDCLOCK_H

#include "config.h"

#define FASTLED_ESP32_I2S true
#include <FastLED.h>

class LedClock {
public:
    LedClock();
    ~LedClock();

    void setup();

    void checkPanel();

    void indicateConnecting(const CRGB& color);

    void update();

    void setBrightness(uint8_t scale);

    void setColor(uint32_t color);
    void setLeds(const uint32_t* colors);

    void showDigits(uint16_t digits, const CRGB& color, bool leadingZero);
    void toggleColon(const CRGB& color);

    void showSpinner(uint8_t position, const CRGB& color);

    void showStock(float price, const CRGB& color);

private:
    CRGBArray<NUM_LEDS> m_leds;

    bool m_colon;
};

extern LedClock leds;

#endif // LEDCLOCK_H
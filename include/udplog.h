#ifndef UDPLOG_H
#define UDPLOG_H

#include <WifiUdp.h>

class UdpLog {
public:
    UdpLog();
    ~UdpLog();

    void setEndpoint(IPAddress ip, uint16_t port);
    void enableLog(bool enable);

    void send(char *fmt, ...);

private:
    IPAddress m_ip;
    uint16_t m_port;

    bool m_enabled;

    WiFiUDP m_udp;
};

extern UdpLog logger;

#endif // UDPLOG_H
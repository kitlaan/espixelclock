#ifndef WIFISETUP_H
#define WIFISETUP_H

class String;

const String& getDefaultHostname();

void setupWifi(const String& hostname, const String& ssid, const String& password);

#endif // WIFISETUP_H
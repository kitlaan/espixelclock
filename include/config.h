#ifndef CONFIG_H
#define CONFIG_H

#include <WString.h>

#define NUM_LEDS 86
#define NUM_OUTER_LEDS 36

///////////////////////////////////////////////////////////

class Runtime {
public:
    Runtime();
    ~Runtime();

    void setBrightness(uint8_t scale);

    enum RunMode {
        MODE_NONE,
        MODE_CLOCK,
        MODE_SPINNER,
        MODE_RAW,
        MODE_STOCK,
    };

    void setMode(RunMode mode);
    RunMode getMode() const;

    // MODE_CLOCK
    void setClockData(uint32_t color);
    uint32_t getClockColor() const;

    // MODE_SPINNER
    void setSpinnerData(uint32_t color, uint32_t speed);
    uint32_t getSpinnerColor() const;
    uint32_t getSpinnerSpeed() const;

    // MODE_RAW
    void setRawData(const uint32_t data[NUM_LEDS], uint32_t duration);
    const uint32_t* getRawData() const;
    uint32_t getRawDuration() const;

    // MODE_STOCK
    void setStockData(const String& symbol);
    const String& getStockSymbol() const;

private:
    RunMode m_mode;

    struct {
        uint32_t color;
    } m_clockData;

    struct {
        uint32_t color;
        uint32_t speed;
    } m_spinnerData;

    struct {
        uint32_t data[NUM_LEDS];
        uint32_t duration;
    } m_rawData;

    struct {
        String symbol;
    } m_stockData;
};

extern Runtime runtime;

///////////////////////////////////////////////////////////

class Config {
public:
    Config();
    ~Config();

    void defaults();

    void restore();
    void persist();

    bool hasChange() const;

    enum ConfigKey {
        WIFI_SSID,
        WIFI_PASS,

        TIMEZONE,

        NTPSERVER1,
        NTPSERVER2,

        HOSTNAME,

        BRIGHTNESS,

        MAX_KEYS
    };

    const String& get(ConfigKey key) const;
    const String& set(ConfigKey key, const char* value);

    const char* getKey(ConfigKey key) const;
    const char* getDescription(ConfigKey key) const;

private:
    bool m_changed;

    struct ConfigEntry {
        const char* key;
        const char* value;
        const char* description;
    };
    struct ConfigSetting {
        String      value;
        bool        changed;
    };

    static ConfigEntry m_keys[];
    ConfigSetting m_pref[MAX_KEYS];
};

extern Config config;

#endif // CONFIG_H
# ESPixelClock (Simple)

A simple display using the [ESPixel Clock][kickstarter] with on-board ESP32.

This [board][spec] has 86 WS2812B (mini-3535; NeoPixel) RGB LEDs arranged to replicate a
standard 4-digit 8-segment display. It also has an on-board RTC (DS3231M; I2C 0x68)
and temperature/humidity sensor (SHT21; I2C 0x40).

[kickstarter]: https://www.kickstarter.com/projects/etap/espixel-clock
[spec]: https://github.com/mattncsu/ESPixelClock

# Features

This is much simplified implementation than the reference code, featuring:

* Clock
* Stock tracker
* OTA via HTTP (`/update`) and/or PlatformIO OTA

There's still a lot left unfinished...

* actual RTC usage
* temperature/humidity display (and calibration)

# Hardware Notes

1. Solder on some headers and apply 5V to the board. (USB power
   seems to work fine.)
2. By default, the board will have no SSID configured. This will cause
   it to go into OpenAP mode (SSID "ESPixelClock ##") with a password
   of "espixelclock".
3. Configuration can be done via a REST endpoint (see below).

A case design is included in the `enclosure` directory, made for
laser cutting out of 3mm wood.

# Setup and Usage Notes

When building, add customizations in `platformio_local.ini`. For example,

```
[common_env_data]
build_flags =
    -D FINNHUB_TOKEN=XYZ

[env:esp32dev]
upload_protocol = espota
upload_port = espixelclock-00
;upload_port = 192.168.3.1
```

To change behavior live, there are some REST endpoints that can be used:

* `/status` -- GET some state of the device for debugging
* `/config` -- GET a JSON of the current configuration
* `/config` -- POST a JSON of new configuration
* `/mode/raw` -- POST a JSON to change to raw display mode
* `/mode/clock` -- POST a JSON to change to clock display mode
* `/mode/spinner` -- POST a JSON to change to a spinner display mode
* `/mode/stock` -- POST a JSON to change to a stock tracker display mode

### Config

To query the current configuration...

```
$ curl -s "http://espixelclock-00.local/config" | jq
{
  "ssid": "myssid",
  "timezone": "EST5EDT,M3.2.0,M11.1.0,",
  "ntpserver1": "time.google.com",
  "ntpserver2": "pool.ntp.org",
  "hostname": "espixelclock-00",
  "brightness": "12"
}
```

Possible config values are documented in `config.cpp`.
To set values...

```
$ curl -H "Content-Type: application/json" \
    -d "{ 'ssid': 'newssid', 'password': 'newpassword' }" \
    http://espixelclock-00.local/config
```

### Raw Mode
data
duration

To switch to raw display mode, send:

* data -- array of 86 decimal color values
* duration -- amount of seconds to display before reverting to previous mode

```
$ curl -H "Content-Type: application/json" \
    -d "{ 'data': [ ... ], 'duration': 10 }" \
    http://espixelclock-00.local/mode/raw
```

### Clock Mode

To switch to clock mode, send:

* color -- decimal value of color

```
$ curl -H "Content-Type: application/json" \
    -d "{ 'color': $((16#FF0000)) }" \
    http://espixelclock-00.local/mode/clock
```
### Spinner Mode

To switch to progress spinner mode, send:

* color -- decimal value of color
* speed -- delay between updates (in milliseconds)

```
$ curl -H "Content-Type: application/json" \
    -d "{ 'color': $((16#FF0000)), 'speed': 50 }" \
    http://espixelclock-00.local/mode/spinner
```

### Stock Tracker Mode

To switch to stock tracker mode, send the stock symbol:

* symbol -- stock symbol string

```
$ curl -H "Content-Type: application/json" \
    -d "{ 'symbol': 'AAPL' }" \
    http://espixelclock-00.local/mode/stock
```

Increases show as green, decreases as red. The saturation of
color increases depending on proximity to the current high/low.
#include "ledclock.h"

LedClock leds;

LedClock::LedClock()
    : m_colon(false)
{
}

LedClock::~LedClock()
{
}

void LedClock::setup()
{
    FastLED.addLeds<WS2812B, 18, GRB>(m_leds, m_leds.size());
    FastLED.setCorrection(TypicalSMD5050);

    FastLED.setMaxPowerInVoltsAndMilliamps(5, 2000);

    FastLED.setBrightness(8);
}

void LedClock::checkPanel()
{
    m_leds = CRGB::Red;
    FastLED.delay(1000);

    m_leds = CRGB::Green;
    FastLED.delay(1000);

    m_leds = CRGB::Blue;
    FastLED.delay(1000);

    m_leds = CRGB::Black;
    FastLED.delay(100);

    FastLED.clear(true);
}

void LedClock::indicateConnecting(const CRGB& color)
{
    m_leds[0] = color;
    FastLED.show();
}

void LedClock::update()
{
    FastLED.show();
    FastLED.delay(10);
    // TODO: compute how long to actually delay here, in case other code took some of this time
}

void LedClock::setBrightness(uint8_t scale)
{
    FastLED.setBrightness(scale);
}

void LedClock::setColor(uint32_t color)
{
    m_leds = color;
}

void LedClock::setLeds(const uint32_t* colors)
{
    for (size_t ix = 0; ix < NUM_LEDS; ++ix) {
        m_leds[ix] = colors[ix];
    }
}

void LedClock::showDigits(uint16_t digits, const CRGB& color, bool leadingZero)
{
    const size_t offsets[] = { 63, 42, 21, 0 };
    const CRGB colon[2] = { m_leds[84], m_leds[85] };

    m_leds = CRGB::Black;

    if (!color) {
        return;
    }

    m_leds[84] = colon[0];
    m_leds[85] = colon[1];

    for (size_t i = 0; i < 4; ++i) {
        if (!leadingZero && digits == 0 && i != 0) {
            break;
        }

        const size_t off = offsets[i];

        const char d = digits % 10;
        digits /= 10;

        if (d != 1 && d != 4) {
            m_leds[off + 0] = m_leds[off + 1] = m_leds[off + 2] = color;
        }
        if (d != 5 && d != 6) {
            m_leds[off + 3] = m_leds[off + 4] = m_leds[off + 5] = color;
        }
        if (d != 2) {
            m_leds[off + 6] = m_leds[off + 7] = m_leds[off + 8] = color;
        }
        if (d != 1 && d != 4 && d != 7) {
            m_leds[off + 9] = m_leds[off + 10] = m_leds[off + 11] = color;
        }
        if (d == 0 || d == 2 || d == 6 || d == 8) {
            m_leds[off + 12] = m_leds[off + 13] = m_leds[off + 14] = color;
        }
        if (d != 1 && d != 2 && d != 3 && d != 7) {
            m_leds[off + 15] = m_leds[off + 16] = m_leds[off + 17] = color;
        }
        if (d != 0 && d != 1 && d != 7) {
            m_leds[off + 18] = m_leds[off + 19] = m_leds[off + 20] = color;
        }
    }
}

void LedClock::toggleColon(const CRGB& color)
{
    m_colon = !m_colon;

    m_leds[84] = m_leds[85] = m_colon ? color : CRGB::Black;
}

void LedClock::showSpinner(uint8_t position, const CRGB& color)
{
    const uint8_t outerIndexes[] = {
        0, 1, 2, 21, 22, 23, 42, 43, 44, 63, 64, 65,
        66, 67, 68, 69, 70, 71,
        72, 73, 74, 51, 52, 53, 30, 31, 32, 9, 10, 11,
        12, 13, 14, 15, 16, 17
    };

    m_leds = CRGB::Black;

    for (size_t ix = 0; ix < 3; ++ix) {
        const size_t oix = (position + ix) % NUM_OUTER_LEDS;
        m_leds[outerIndexes[oix]] = color;
    }
}

void LedClock::showStock(float price, const CRGB& color)
{
    if (price <= 0.0 || price >= 10000.0) {
        m_leds = CRGB::Black;
        //m_leds[ 0 + 18] = m_leds[ 0 + 19] = m_leds[ 0 + 20] = color;
        m_leds[21 + 18] = m_leds[21 + 19] = m_leds[21 + 20] = color;
        m_leds[42 + 18] = m_leds[42 + 19] = m_leds[42 + 20] = color;
        //m_leds[63 + 18] = m_leds[63 + 19] = m_leds[63 + 20] = color;
        return;
    }

    bool decimal = false;
    uint16_t digits;

    if (price < 100.0) {
        digits = (uint16_t)(price * 100);
        decimal = true;
    }
    else {
        digits = (uint16_t)price;
    }

    showDigits(digits, color, false);
    if (decimal) {
        m_leds[85] = color;
    }
}
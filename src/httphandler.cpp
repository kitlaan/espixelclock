#include "httphandler.h"

#include "config.h"
#include "sensors.h"
#include "stock.h"

#include <ESPAsyncWebServer.h>
#include <AsyncJson.h>


void setupHttp(AsyncWebServer& server)
{
    server.on("/status", HTTP_GET, [](AsyncWebServerRequest *request) {
        AsyncResponseStream  *response = request->beginResponseStream("text/plain");

        response->printf("Clock Digits: %u\n", wallclock.getTimeDigits());
        response->printf("Heap: %u\n", ESP.getFreeHeap());
        response->printf("Market State: %s %f %f\n", stock.getMarketOpen() ? "open" : "closed", stock.getPrice(), stock.getScale());
        response->printf("Market HTTP: %d\n", stock.getLastHttp());
        response->printf("Weather Humidity: %f\n", weather.getHumidity());
        response->printf("Weather Temperature: %f\n", weather.getTemperature());

        request->send(response);
    });

    server.on("/config", HTTP_GET, [](AsyncWebServerRequest *request) {
        AsyncResponseStream  *response = request->beginResponseStream("application/json");
        DynamicJsonDocument root(1024);

        for (size_t ix = 0; ix < Config::MAX_KEYS; ++ix) {
            Config::ConfigKey key = static_cast<Config::ConfigKey>(ix);
            if (key != Config::WIFI_PASS) {
                root[config.getKey(key)] = config.get(key);
            }
        }

        serializeJson(root, *response);
        request->send(response);
    });

    // Configure random settings
    server.addHandler(new AsyncCallbackJsonWebHandler(
              "/config", [](AsyncWebServerRequest *request, JsonVariant &json) {
        JsonObjectConst jsonObj = json.as<JsonObject>();

        for (size_t ix = 0; ix < Config::MAX_KEYS; ++ix) {
            Config::ConfigKey key = static_cast<Config::ConfigKey>(ix);
            if (jsonObj.containsKey(config.getKey(key))) {
                config.set(key, jsonObj[config.getKey(key)].as<String>().c_str());
            }
        }

        request->send(200);
    }, 4096));

    // Directly configure the LED values to show
    server.addHandler(new AsyncCallbackJsonWebHandler(
              "/mode/raw", [](AsyncWebServerRequest *request, JsonVariant &json) {
        int code = 400;

        if (json.isNull()) {
            code = 200;
        }
        else {
            JsonObjectConst jsonObj = json.as<JsonObject>();

            JsonArrayConst jsonArr = jsonObj["data"];
            if (jsonArr.size() == NUM_LEDS) {
                uint32_t colors[NUM_LEDS];
                for (size_t ix = 0; ix < NUM_LEDS; ++ix) {
                    colors[ix] = jsonArr[ix];
                }

                runtime.setRawData(colors,
                                   jsonObj["duration"] | 0);
                code = 200;
            }
        }

        if (code == 200) {
            runtime.setMode(Runtime::MODE_RAW);
        }
        request->send(code);
    }, 4096));

    // Clock mode
    server.addHandler(new AsyncCallbackJsonWebHandler(
              "/mode/clock", [](AsyncWebServerRequest *request, JsonVariant &json) {
        int code = 400;

        if (json.isNull()) {
            code = 200;
        }
        else {
            JsonObjectConst jsonObj = json.as<JsonObject>();
            if (!jsonObj.isNull()) {
                runtime.setClockData(jsonObj["color"] | 0xFFFFFFFF);
                code = 200;
            }
        }

        if (code == 200) {
            runtime.setMode(Runtime::MODE_CLOCK);
        }
        request->send(code);
    }));

    // Spinner mode
    server.addHandler(new AsyncCallbackJsonWebHandler(
              "/mode/spinner", [](AsyncWebServerRequest *request, JsonVariant &json) {
        int code = 400;

        if (json.isNull()) {
            code = 200;
        }
        else {
            JsonObjectConst jsonObj = json.as<JsonObject>();
            if (!jsonObj.isNull()) {
                runtime.setSpinnerData(jsonObj["color"] | 0xFFFFFFFF,
                                       jsonObj["speed"] | 0xFFFFFFFF);
                code = 200;
            }
        }

        if (code == 200) {
            runtime.setMode(Runtime::MODE_SPINNER);
        }
        request->send(code);
    }));

    // Stock mode
    server.addHandler(new AsyncCallbackJsonWebHandler(
              "/mode/stock", [](AsyncWebServerRequest *request, JsonVariant &json) {
        int code = 400;

        if (json.isNull()) {
            code = 200;
        }
        else {
            JsonObjectConst jsonObj = json.as<JsonObject>();
            if (!jsonObj.isNull()) {
                runtime.setStockData(jsonObj["symbol"]);
                code = 200;
            }
        }

        if (code == 200) {
            runtime.setMode(Runtime::MODE_STOCK);
        }
        request->send(code);
    }));
}

// https://github.com/me-no-dev/ESPAsyncWebServer/issues/510
// schedule_function to do WRITES
// so how do we protect other web reads while loop is running?

/*
 * switch to progress bar mode
 * POST /mode/progress/h/0 (0..2)
 * { 'color': uint32_t, 'percent': 100 }
 * POST /mode/progress/v/0 (0..7)
 * ... TODO: or should it be array of objects, with 'id': 0
 * 
 * write digits
 * POST /mode/digits
 * { 'value': 8888, 'topdot': true, 'botdot': true, 'color': uint32_t }
 * 
 * show temp/humid
 * 
 * clock w/ progress?
 * clock w/ busy?
 * clock w/ stock coloring?
 * 
 * countdown
 * stopwatch
 */
#include "sensors.h"

#include <WiFi.h>

WallClock wallclock;

WallClock::WallClock()
    : m_needsUpdate(true)
    , m_isSynced(false)
    , m_24h(true)
{
}

WallClock::~WallClock()
{
}

void WallClock::setup()
{
    m_rtc.begin();

    // TODO: load from RTC if it has a valid value
}

bool WallClock::isSynced() const
{
    return m_isSynced;
}

void WallClock::setTimeZone(const String &tz)
{
    m_tz = tz;

    m_needsUpdate = true;
}

void WallClock::setEndpoints(const String &host1, const String &host2)
{
    m_host1 = host1;
    m_host2 = host2;

    m_needsUpdate = true;
}

void WallClock::set24h(bool enable)
{
    m_24h = enable;
}

void WallClock::refresh()
{
    if (WiFi.status() != WL_CONNECTED) {
        return;
    }

    if (m_needsUpdate) {
        m_needsUpdate = false;
        configTzTime(m_tz.c_str(), m_host1.c_str(), m_host2.c_str());
    }

    struct tm timeinfo;
    if (!getLocalTime(&timeinfo)) {
        return;
    }

    m_isSynced = true;
    m_rtc.adjust(DateTime(timeinfo.tm_year + 1900,
                          timeinfo.tm_mon + 1,
                          timeinfo.tm_mday,
                          timeinfo.tm_hour,
                          timeinfo.tm_min,
                          timeinfo.tm_sec));
}

uint16_t WallClock::getTimeDigits()
{
    struct tm timeinfo;
    if (!m_isSynced || !getLocalTime(&timeinfo)) {
        return 8888;
    }

    uint8_t hour = timeinfo.tm_hour;
    if (!m_24h) {
        hour %= 12;
        if (hour == 0) {
            hour = 12;
        }
    }
    return (hour * 100) + timeinfo.tm_min;
}

bool WallClock::isMarketOpen()
{
    struct tm timeinfo;
    if (!m_isSynced || !getLocalTime(&timeinfo)) {
        return false;
    }

    if (timeinfo.tm_wday == 0 || timeinfo.tm_wday == 6) {
        return false;
    }
    if (timeinfo.tm_hour == 9 && timeinfo.tm_min >= 30) {
        return true;
    }
    if (timeinfo.tm_hour > 9 && timeinfo.tm_hour < 16) {
        return true;
    }
    return false;
}

///////////////////////////////////////////////////////////

Weather weather;

Weather::Weather()
    : m_sensor(HTU21D_RES_RH12_TEMP14) // TODO: do I need this high resolution? HTU21D_RES_RH11_TEMP11
    , m_temp(NAN)
    , m_humid(NAN)
{
}

Weather::~Weather()
{
}

void Weather::setup()
{
    m_sensor.begin();
}

void Weather::refresh()
{
    float humid = m_sensor.readHumidity(HTU21D_TRIGGER_HUMD_MEASURE_NOHOLD);
    float temp = m_sensor.readTemperature(HTU21D_TRIGGER_TEMP_MEASURE_NOHOLD);

    m_humid = (humid != HTU21D_ERROR) ? humid : NAN;
    m_temp = (temp != HTU21D_ERROR) ? temp : NAN;
}

float Weather::getTemperature() const
{
    return m_temp;
    // TODO: temperature offset
    // TODO: C vs F
}

float Weather::getHumidity() const
{
    return m_humid;
}

// TODO: should we toggle setHeater every once in a while?
#include <Arduino.h>

#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
//#include <AsyncElegantOTA.h>
#include <ArduinoOTA.h>

#include "wifisetup.h"
#include "httphandler.h"
#include "config.h"
#include "ledclock.h"
#include "sensors.h"
#include "stock.h"

AsyncWebServer server(80);

void setup() {
    leds.setup();
    wallclock.setup();
    weather.setup();

    leds.checkPanel();

    config.restore();

    setupWifi(config.get(Config::HOSTNAME), config.get(Config::WIFI_SSID), config.get(Config::WIFI_PASS));
    setupHttp(server);

    wallclock.setTimeZone(config.get(Config::TIMEZONE));
    wallclock.setEndpoints(config.get(Config::NTPSERVER1), config.get(Config::NTPSERVER2));

    ArduinoOTA.setHostname(config.get(Config::HOSTNAME).c_str());
    ArduinoOTA.begin();

    //AsyncElegantOTA.begin(&server);

    server.begin();

    wallclock.refresh();
    weather.refresh();
}

void loop() {
    ArduinoOTA.handle();

    //AsyncElegantOTA.loop();

    if (Update.isRunning()) {
        return;
    }

    leds.update();

    static bool firstLoop = true;
    if (config.hasChange() || firstLoop) {
        firstLoop = false;
        config.persist();

        leds.setBrightness(config.get(Config::BRIGHTNESS).toInt());
    }

    static Runtime::RunMode lastMode = Runtime::MODE_NONE;
    Runtime::RunMode currMode = runtime.getMode();
    switch (currMode) {
        case Runtime::MODE_CLOCK:
        {
            static CEveryNSeconds showColon(1);
            static CEveryNSeconds showDigits(5);

            static uint32_t lastColor = 0;
            uint32_t currColor = runtime.getClockColor();

            if (WiFi.getMode() != WIFI_STA) {
                currColor = 0x999900; // yellow
            }
            else if (lastMode != currMode || lastColor != currColor) {
                showColon.trigger();
                showDigits.trigger();
                lastColor = currColor;
            }

            if (showColon) {
                leds.toggleColon(currColor);
            }
            if (showDigits) {
                leds.showDigits(wallclock.getTimeDigits(), currColor, true);
            }

            break;
        }

        case Runtime::MODE_NONE:
        case Runtime::MODE_RAW:
        {
            static uint32_t secondsRemaining = 0;
            static Runtime::RunMode previousMode = Runtime::MODE_NONE;

            if (lastMode != currMode) {
                previousMode = lastMode;

                if (currMode == Runtime::MODE_RAW) {
                    leds.setLeds(runtime.getRawData());
                    secondsRemaining = runtime.getRawDuration();
                }
                else {
                    leds.setColor(0);
                    secondsRemaining = 0;
                }
            }

            if (secondsRemaining) {
                EVERY_N_SECONDS(1) {
                    secondsRemaining--;
                    if (!secondsRemaining) {
                        runtime.setMode(previousMode);
                    }
                }
            }

            break;
        }

        case Runtime::MODE_SPINNER:
        {
            static uint8_t position = 0;

            uint32_t color = runtime.getSpinnerColor();
            uint32_t speed = runtime.getSpinnerSpeed();

            EVERY_N_MILLISECONDS_I(spinnerSpeed, speed) {
                leds.showSpinner(position++, color);
                position %= NUM_OUTER_LEDS;
                spinnerSpeed.setPeriod(speed);
            }

            break;
        }

        case Runtime::MODE_STOCK:
        {
            static CEveryNMinutes refreshStock(1);

            if (lastMode != currMode) {
                refreshStock.trigger();
                stock.setSymbol(runtime.getStockSymbol());

                leds.setColor(0);
                leds.update();
            }

            if (refreshStock) {
                stock.fetch();

                uint32_t color;
                if (stock.getFailed()) {
                    color = 0x0000FF;
                }
                else if (stock.getMarketOpen()) {
                    float scale = stock.getScale();
                    if (scale >= 0.0) {
                        color = 0x00A000 + ((uint32_t)(0x5F * scale) << 8);
                    }
                    else {
                        color = 0xA00000 - ((uint32_t)(0x5F * scale) << 16);
                    }
                }
                else {
                    color = 0x909090;
                }
                leds.showStock(stock.getPrice(), color);
            }

            break;
        }
    }
    lastMode = currMode;

    EVERY_N_MINUTES(5) {
        weather.refresh();

        if (!wallclock.isSynced()) {
            wallclock.refresh();
        }
    }

    EVERY_N_HOURS(12) {
        wallclock.refresh();
    }
}
#include "wifisetup.h"

#include "ledclock.h"

#include <WiFi.h>
#include <ESPmDNS.h>

static String s_hostname;

const String& getDefaultHostname()
{
    if (s_hostname.isEmpty()) {
        s_hostname = "espixelclock-";
        s_hostname += String((uint8_t)(ESP.getEfuseMac() >> 40), HEX);
    }

    return s_hostname;
}

void setupWifi(const String& hostname, const String& ssid, const String& password)
{
    WiFi.persistent(false);

    WiFi.setHostname(hostname.c_str());
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid.c_str(), password.c_str());

    leds.indicateConnecting(CRGB::White);

    int i = 0;
    while (WiFi.status() != WL_CONNECTED && i++ < 200) {
        leds.indicateConnecting((i % 2 == 0) ? CRGB::Yellow : CRGB::Black);

        delay(100);
    }

    if (WiFi.status() != WL_CONNECTED) {
        leds.indicateConnecting(CRGB::Red);

        WiFi.mode(WIFI_OFF);
        delay(100);

        String aphostname("ESPixelClock ");
        aphostname += String((uint8_t)(ESP.getEfuseMac() >> 40), HEX);

        WiFi.mode(WIFI_AP);
        WiFi.softAP(aphostname.c_str(), "espixelclock");
        WiFi.softAPConfig(IPAddress(192, 168, 3, 1), IPAddress(192, 168, 3, 1), IPAddress(255, 255, 255, 0));
    }
    else {
        leds.indicateConnecting(CRGB::Black);
    }

    MDNS.begin(hostname.c_str());
    MDNS.addService("http", "tcp", 80);
}
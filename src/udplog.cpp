#include "udplog.h"

UdpLog logger;

UdpLog::UdpLog()
    : m_enabled(false)
{
}

UdpLog::~UdpLog()
{
}

void UdpLog::setEndpoint(IPAddress ip, uint16_t port)
{
    m_ip = ip;
    m_port = port;
}

void UdpLog::enableLog(bool enable)
{
    m_enabled = enable;
    if (enable) {
        // set up the socket
        m_udp.beginPacket();
    }
}

void UdpLog::send(char *fmt, ...)
{
    if (!m_enabled) {
        return;
    }

    char buf[128];

    va_list args;
    va_start(args, fmt);
    int len = vsnprintf(buf, sizeof(buf), fmt, args);
    if (len > sizeof(buf)) {
        len = sizeof(buf);
    }
    va_end(args);

    if (m_udp.beginPacket(m_ip, m_port)) {
        m_udp.write((uint8_t*)buf, len);
        m_udp.endPacket();
    }
}
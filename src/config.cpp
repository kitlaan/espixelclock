
#include "config.h"
#include "wifisetup.h"

#include <Preferences.h>

#define CONFIG_PREF_KEY "ESPixelClock"

///////////////////////////////////////////////////////////

Runtime runtime;

Runtime::Runtime()
    : m_mode(MODE_CLOCK)
    , m_clockData{ 0x00FF0000 }
    , m_spinnerData{ 0x00FF0000, 100 }
    , m_rawData{ {0}, 0 }
{
}

Runtime::~Runtime()
{
}

void Runtime::setMode(RunMode mode)
{
    m_mode = mode;
}
Runtime::RunMode Runtime::getMode() const
{
    return m_mode;
}

// MODE_CLOCK
void Runtime::setClockData(uint32_t color)
{
    if (color <= 0x00FFFFFF) {
        m_clockData.color = color;
    }
}
uint32_t Runtime::getClockColor() const
{
    return m_clockData.color;
}

// MODE_SPINNER
void Runtime::setSpinnerData(uint32_t color, uint32_t speed)
{
    if (color <= 0x00FFFFFF) {
        m_spinnerData.color = color;
    }
    if (speed != 0xFFFFFFFF) {
        m_spinnerData.speed = speed;
    }
}
uint32_t Runtime::getSpinnerColor() const
{
    return m_spinnerData.color;
}
uint32_t Runtime::getSpinnerSpeed() const
{
    return m_spinnerData.speed;
}

// MODE_RAW
void Runtime::setRawData(const uint32_t data[NUM_LEDS], uint32_t duration)
{
    memcpy(m_rawData.data, data, sizeof(m_rawData.data));
    m_rawData.duration = duration;
}
const uint32_t* Runtime::getRawData() const
{
    return m_rawData.data;
}
uint32_t Runtime::getRawDuration() const
{
    return m_rawData.duration;
}

// MODE_STOCK
void Runtime::setStockData(const String& symbol)
{
    if (!symbol.isEmpty()) {
        m_stockData.symbol = symbol;
    }
}
const String& Runtime::getStockSymbol() const
{
    return m_stockData.symbol;
}

///////////////////////////////////////////////////////////

Config config;

// static
Config::ConfigEntry Config::m_keys[] = {
    { "ssid",         "none",                       "WiFi AP SSID" },
    { "password",     "none",                       "WiFi AP Password" },

    { "timezone",     "EST5EDT,M3.2.0,M11.1.0",     "Time Zone" },

    { "ntpserver1",   "time.google.com",            "NTP Server 1" },
    { "ntpserver2",   "pool.ntp.org",               "NTP Server 2" },

    { "hostname",     getDefaultHostname().c_str(), "Hostname" },

    { "brightness",   "8",                          "Display brightness" },
};

Config::Config()
{
}

Config::~Config()
{
}

void Config::defaults()
{
    static_assert(MAX_KEYS == sizeof(m_keys)/sizeof(m_keys[0]), "invalid number of keys configured");

    m_changed = true;

    for (size_t ix = 0; ix < MAX_KEYS; ++ix) {
        m_pref[ix].value = m_keys[ix].value;
        m_pref[ix].changed = true;
    }
}

void Config::restore()
{
    m_changed = false;

    Preferences prefs;
    prefs.begin(CONFIG_PREF_KEY, true);

    for (size_t ix = 0; ix < MAX_KEYS; ++ix) {
        m_pref[ix].value = prefs.getString(m_keys[ix].key, m_keys[ix].value);
        m_pref[ix].changed = false;
    }

    prefs.end();
}

void Config::persist()
{
    if (!m_changed) {
        return;
    }
    m_changed = false;

    Preferences prefs;
    prefs.begin(CONFIG_PREF_KEY, false);

    for (size_t ix = 0; ix < MAX_KEYS; ++ix) {
        if (m_pref[ix].changed) {
            prefs.putString(m_keys[ix].key, m_pref[ix].value);
            m_pref[ix].changed = false;
        }
    }

    prefs.end();
}

bool Config::hasChange() const
{
    return m_changed;
}

const String& Config::get(ConfigKey key) const
{
    return m_pref[key].value;
}

const String& Config::set(ConfigKey key, const char* value)
{
    if (m_pref[key].value != value) {
        m_pref[key].value = value;
        m_pref[key].changed = true;

        m_changed = true;
    }
    return m_pref[key].value;
}

const char* Config::getKey(ConfigKey key) const
{
    return m_keys[key].key;
}

const char* Config::getDescription(ConfigKey key) const
{
    return m_keys[key].description;
}
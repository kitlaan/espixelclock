#include "stock.h"

#include "sensors.h"

#include <HTTPClient.h>
#include <ArduinoJson.h>

#define TOSTRING_(x) #x
#define TOSTRING(x) TOSTRING_(x)

#if !defined(FINNHUB_TOKEN)
#error "FINNHUB_TOKEN not set"
#endif


StockTracker stock;

StockTracker::StockTracker()
    : m_symbol()
    , m_failCount(0)
    , m_sameCount(0)
    , m_marketOpen(false)
    , m_lastHttpCode(0)
    , m_prevData{0.0, 0.0, 0.0, 0.0, 0.0}
    , m_currData{0.0, 0.0, 0.0, 0.0, 0.0}
{
}

StockTracker::~StockTracker()
{
}

void StockTracker::setSymbol(const String& symbol)
{
    if (symbol != m_symbol) {
        m_symbol = symbol;
        m_lastHttpCode = 0;
        m_prevData = {0.0, 0.0, 0.0, 0.0, 0.0};
        m_currData = {0.0, 0.0, 0.0, 0.0, 0.0};
    }

    m_marketOpen = false;
    m_failCount = 0;
    m_sameCount = 0;
}

bool StockTracker::fetch()
{
    // allow fetches to occur when the market is freshly open
    bool marketOpen = wallclock.isMarketOpen();
    if (marketOpen != m_marketOpen) {
        m_marketOpen = marketOpen;
        if (marketOpen) {
            m_failCount = 0;
            m_sameCount = 0;
        }
    }

    // if we've never fetched anything, let a query through
    if (m_failCount >= 5 || m_sameCount >= 5 ||
            m_symbol.isEmpty() ||
            (!m_marketOpen && m_prevData.prevClosePrice != 0.0)) {
        return false;
    }

    String url = "https://finnhub.io/api/v1/quote?symbol=";
    url += m_symbol;
    url += "&token=";
    url += TOSTRING(FINNHUB_TOKEN);

    HTTPClient client;
    client.begin(url);

    int httpCode = client.GET();
    if (httpCode == 200) {
        StaticJsonDocument<128> doc;
        deserializeJson(doc, client.getString());

        m_prevData = m_currData;

        m_currData.prevClosePrice = doc["pc"];
        m_currData.openPrice      = doc["o"];
        m_currData.currPrice      = doc["c"];
        m_currData.highPrice      = doc["h"];
        m_currData.lowPrice       = doc["l"];

        if (0 == memcmp(&m_prevData, &m_currData, sizeof(m_prevData))) {
            // find holidays... what are the chances that the data stays identical across 5 fetches?
            m_sameCount++;
        }
        else {
            m_sameCount = 0;
        }

        if (0.0 == m_prevData.prevClosePrice) {
            // reset the very first fetch so we don't have a spurious double fetch
            m_prevData = m_currData;
        }

        m_failCount = 0;
    }

    client.end();

    m_lastHttpCode = httpCode;
    return httpCode == 200;
}

float StockTracker::getPrice() const
{
    return m_currData.currPrice;
}

float StockTracker::getScale() const
{
    if (m_currData.currPrice >= m_currData.prevClosePrice) {
        float delta = m_currData.currPrice - m_currData.prevClosePrice;
        float denom = m_currData.highPrice - m_currData.prevClosePrice;
        return (denom == 0) ? 1.0 : (delta / denom);
    }
    else {
        float delta = m_currData.prevClosePrice - m_currData.currPrice;
        float denom = m_currData.prevClosePrice - m_currData.lowPrice;
        return (denom == 0) ? -1.0 : -(delta / denom);
    }
}

bool StockTracker::getFailed() const
{
    return m_failCount > 0;
}

bool StockTracker::getMarketOpen() const
{
    return m_marketOpen && m_sameCount < 5;
}

 int StockTracker::getLastHttp() const
 {
    return m_lastHttpCode;
}
# Enclosure Designs

### 3mm Wood

Design for laser cutting out of 3mm wood.
The files include a 0.15mm kerf.

* 1x box_front.dxf
* 2x box_top.dxf
* 2x box_side.dxf
* 2x box_brace.dxf
* 2x box_brace_shim.dxf

You should also 3D print a [diffuser][4218698].

[4218698]: https://www.thingiverse.com/thing:4218698

The box itself should be press-fit together; no glue necessary.

The `box_brace` and `box_brace_shim` are glued together, and then
together glued into the box to hold the components against the front.

Thus the order is: box_front, acrylic, diffuser, PCB.